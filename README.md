# storm-helloworld

#### 介绍
storm-helloworld

#### 软件架构
软件架构说明


#### 部署一个storm集群

1.  安装Java 7和Pythong 2.6.6

2.  下载storm安装包，解压缩，重命名，配置环境变量
tar -zxvf apache-storm-1.1.0.tar.gz
mv apache-storm-1.1.0 storm

vi   ~/.bashrc
export JAVA_HOME=/usr/java/latest
export ZOOKEEPER_HOME=/usr/local/zookeeper
export SCALA_HOME=/usr/local/scala
export STORM_HOME=/usr/local/storm

export PATH=$PATH:$JAVA_HOME/bin:$ZOOKEEPER_HOME/bin:$SCALA_HOME/bin:$STORM_HOME/bin
source  ~/.bashrc

3.  修改storm配置文件
mkdir /var/storm

vi /usr/local/storm/conf/storm.yaml

storm.zookeeper.servers:
  - "192.168.1.100"
  - "192.168.1.105"
  - "192.168.1.109"

nimbus.seeds: ["192.168.1.100"]

storm.local.dir: "/var/storm"

#slots.ports，指定每个机器上可以启动多少个worker，一个端口号代表一个worker
supervisor.slots.ports:
    - 6700
    - 6701
    - 6702
    - 6703

在另外两台机器安装：
scp ~/.bashrc root@hadoop04:~/
scp -r  storm root@hadoop04:/usr/local/
mkdir /var/storm
source  ~/.bashrc

4.  启动storm集群和ui界面

一个节点，storm nimbus >/dev/null 2>&1 &
三个节点，storm supervisor >/dev/null 2>&1 &
一个节点，storm ui >/dev/null 2>&1 &
我们需要在两个supervisor节点上，启动logview，然后才能看到日志：storm logviewer >/dev/null 2>&1 &

5.  访问一下ui界面，8080端口
http://192.168.1.100:8080/

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
